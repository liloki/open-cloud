package com.github.lyd.common.constants;

/**
 * @author liuyadu
 */
public class ServicesConstants {
    public static final String BASE_SERVICE = "opencloud-base-producer";
    public static final String AUTH_SERVICE = "opencloud-auth-producer";
    public static final String GATEWAY_SERVICE = "opencloud-gateway-producer";
    public static final String ACMS_SERVICE = "opencloud-acms-producer";
    public static final String MSG_SERVICE = "opencloud-msg-producer";
}
