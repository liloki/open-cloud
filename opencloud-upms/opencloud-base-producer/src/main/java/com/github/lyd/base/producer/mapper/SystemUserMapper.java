package com.github.lyd.base.producer.mapper;

import com.github.lyd.base.client.entity.SystemUser;
import com.github.lyd.common.mapper.CrudMapper;
import org.springframework.stereotype.Repository;

/**
 * @author liuyadu
 */
@Repository
public interface SystemUserMapper extends CrudMapper<SystemUser> {

}
